// console.log("Hello!");
// 1.
let num = 2;
let getCube = num ** 3;
console.log(`The cube of ${num} is ${getCube}`);

// 2.
// 258 Washington Ave NW, California 90011
const address = ["258 Washington Ave NW", "California", "90011"];
const [street, city, zipcode] = address;
console.log(`I live in ${street} ${city} ${zipcode}`);

// 3.
const animal = {
	name: "Lolong",
	type: "saltwater crocodile",
	weight: 1075,
	measurement: "20 ft 3 in."
};

const {name, type, weight, measurement} = animal;
console.log(`${name} was a ${type}. He weight at ${weight} kgs with a measurement of ${measurement} `);

// 4.
const numbers = [1,2,3,4,5];
// numbers.forEach((number) => {
// 	console.log (`${number}`)
// });

const numbers1 = (a,b,c,d,e) => numbers.forEach((number) => {console.log(`${number}`)
})
let numberValues = numbers1(1,2,3,4,5);


//5.
class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}
const myDog = new Dog ();
myDog.name = "Hakdog";
myDog.age = 2;
myDog.breed = "Alaskan Malamute";

console.log(myDog);
